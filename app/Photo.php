<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //
    protected $uplode='/images/';

    protected $fillable = [
        'file',
    ];
    
    public function getFileAttributes($photo){
        
        return $this->uplode . $photo;

    }
    public function posts(){
        return $this->hasMany('App\Post');
    }
    
}
