@extends('layouts.admin');
@section('content')
    <h1>Media</h1>
   
    <div class="col">
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Created</th>
                <th>Updated</th>
            </tr>
            </thead>
            <tbody>
                @if ($photos)
                    @foreach ($photos as $photo)
                        <tr>
                            <td>{{$photo->id}}</td>
                            <td><img height="50" width="80" src="/images/{{$photo->file?$photo->file:'no image'}}" alt=""></td>
                            <td>{{$photo->created_at?$photo->created_at->diffForHumans():'no date'}}</td>
                            <td>{{$photo->updated_at?$photo->updated_at->diffForHumans():'no date'}}</td>
                            <td>
                                {!! Form::open(['method'=>'DELETE','action'=>['AdminMediaController@destroy', $photo->id]]) !!}
                                    <div class='form-group'>
                                        {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                                    </div>
                                {!! Form::close() !!}
                            
                            </td>
                            {{-- <td><a href="{{route('admin.categories.edit',$cat->id)}}"><i class="fa fa-edit"> Edit</i></a></td> --}}

                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>{{--show caregory--}}
      
@endsection