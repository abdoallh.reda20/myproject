@extends('layouts.admin')
@section('content')
    <h1>Posts</h1>
    @if (Session::has('delete_post'))
    <div class="alert-danger">{{session('delete_post')}}</div>
    @endif

    <table class="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>User</th>
            <th>Caregory</th>
            <th>Photo</th>
            <th>Title</th>
            <th>Body</th>
            <th>Post</th>
            <th>Comments</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
            @if ($posts)
                @foreach ($posts as $post)
                    <tr>
                        <td>{{$post->id}}</td>
                        <td>{{$post->user->name}}</td>
                        <td>{{$post->category?$post->category->name:'uncaregorized'}}</td>
                        <td><img height="50" width="50" src="/images/{{$post->photo?$post->photo->file:'https://via.placeholder.com/400'}}" alt=""></td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->body}}</td>
                        <td><a href="{{route('home.post',$post->id)}}">View post</a></td>
                        <td><a href="{{route('admin.comments.show',$post->id)}}">View comment</a></td>
                        <td>{{$post->created_at->diffForhumans()}}</td>
                        <td>{{$post->updated_at->diffForhumans()}}</td>
                        <td><a href="{{route('admin.posts.edit',$post->id)}}"><i class="fa fa-edit"> Edit</i></a></td>

                    </tr>
                @endforeach
            @endif
        </tbody>
      </table>


@endsection


