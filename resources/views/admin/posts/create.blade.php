@extends('layouts.admin')
@section('content')


<h1>Create Post</h1>
    <div class="row">
        {!! Form::open(['method'=>'POST','action'=>'AdminPostsController@store','files'=>true ]) !!}
            <div class="form-group">
                {!! Form::label('title','Title:') !!}
                {!! Form::text('title', null ,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('category_id','category') !!}
                {!! Form::select('category_id',[''=>'Choose Categories'] + $cat , null ,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('photo_id','Choose image') !!}
                {!! Form::file('photo_id', null) !!}
            </div>

            <div class="form-group">
                {!! Form::label('body','Description') !!}
                {!! Form::textarea('body',null ,['class'=>'form-control','rows'=>5]) !!}
            </div>

            
            <div class="form-group">
                {!! Form::submit('Create post', ['class'=>'btn btn-primary'])!!}
            </div>
        {!! Form::close()!!}
    </div>

    <div class="row alert-danger">
        @include('includes.form_error')
    </div>    
@endsection