@extends('layouts.admin')


@section('content')

@if (count($replies)>0)

    <h1>replies</h1>
    <table class="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Body</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Post</th>
            <th>Status</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
                @foreach ($replies as $reply)
                    <tr>
                        <td>{{$reply->id}}</td>
                        <td>{{$reply->author}}</td>
                        <td>{{$reply->email}}</td>
                        <td>{{$reply->bady}}</td>
                        <td>{{$reply->created_at->diffForHumans()}}</td>
                        <td>{{$reply->updated_at->diffForHumans()}}</td>
                        <td><a href="{{route('home.post',$reply->comment->post->id)}}"> view Post </a></td>
                        @if ($reply->is_active== 1)
                        <td>
                            {!! Form::open(['method'=>'PATCH','action'=>['CommentRpliesController@update', $reply->id]]) !!}
                              <div class='form-group'>
                                <input type="hidden" name="is_active" value="0">
                                {!! Form::submit('Un approve',['class'=>'btn btn-success']) !!}
                              </div>
                            {!! Form::close() !!}
                        </td>    
                        @else
                        <td>
                            {!! Form::open(['method'=>'PATCH','action'=>['CommentRpliesController@update', $reply->id]]) !!}
                              <input type="hidden" name="is_active" value="1">                               
                              <div class='form-group'>
                                {!! Form::submit('Approve',['class'=>'btn btn-info']) !!}
                              </div>
                            {!! Form::close() !!}
                        </td>    
                        @endif
                        
                        
                        <td>
                            {!! Form::open(['method'=>'DELETE','action'=>['CommentRpliesController@destroy', $reply->id]]) !!}
                              <div class='form-group'>
                                {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                              </div>
                            {!! Form::close() !!}
                        </td>

                    </tr>
                @endforeach
        </tbody>
      </table>
      
          
      @else
      
      <h1 class="text-center">No replies</h1>
      
      @endif


    
@endsection