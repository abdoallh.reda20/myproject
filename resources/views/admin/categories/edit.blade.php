@extends('layouts.admin')
@section('content')
	<h1>Edit Post</h1>
<div class="row">
        {!! Form::model($cat, ['method'=>'PATCH','action'=>['AdminCategoryController@update',$cat->id]]) !!}
			    <div class='form-group'>
			        {!! Form::label('name','name:') !!}
			        {!! Form::text('name',null,['class'=>'form-control']) !!}
			    </div>

			    
			    <div class='form-group'>
			        {!! Form::submit('Update Category',['class'=>'btn btn-primary col-sm-6']) !!}
			    </div>

			{!! Form::close() !!}
						
			{!! Form::open(['method'=>'DELETE','action'=>['AdminCategoryController@destroy', $cat->id] ]) !!}
				<div class="form-group">
					{!! Form::submit('Delete Category', ['class'=>'btn btn-danger col-sm-6'])!!}
				</div>
			{!! Form::close()!!}
</div>{{-- end row --}}
@endsection