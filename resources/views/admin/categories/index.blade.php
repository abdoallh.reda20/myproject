@extends('layouts.admin');
@section('content')
    <h1>Category</h1>
    <div class="col-sm-6">
        {!! Form::open(['method'=>'POST','action'=>'AdminCategoryController@store' ]) !!}
            <div class="form-group">
                {!! Form::label('name','Name:') !!}
                {!! Form::text('name', null ,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Create Category', ['class'=>'btn btn-primary'])!!}
            </div>
        {!! Form::close()!!}
    </div>

    <div class="col-sm-6">
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
                @if ($category)
                    @foreach ($category as $cat)
                        <tr>
                            <td>{{$cat->id}}</td>
                            <td>{{$cat->name}}</td>
                            <td>{{$cat->created_at?$cat->created_at->diffForHumans():'no date'}}</td>
                            <td>{{$cat->updated_at?$cat->updated_at->diffForHumans():'no date'}}</td>
                            <td><a href="{{route('admin.categories.edit',$cat->id)}}"><i class="fa fa-edit"> Edit</i></a></td>

                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>{{--show caregory--}}
      
@endsection