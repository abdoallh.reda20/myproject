@extends('layouts.admin');
@section('content')
@if (Session::has('delete_user'))
<div class="alert-danger">{{session('delete_user')}}</div>
@endif
    <h1> Users </h1>
    
    <table class="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Photo</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Status</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
            @if ($users)
                @foreach ($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td><img height="50" width="50" src="/images/{{$user->photo?$user->photo->file:'https://via.placeholder.com/400'}}" alt=""></td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role->name}}</td>
                        <td>{{$user->is_active==1 ? 'Is Active':'Not Active'}}</td>
                        <td>{{$user->created_at->diffForHumans()}}</td>
                        <td>{{$user->updated_at->diffForHumans()}}</td>
                    <td><a href="{{route('admin.users.edit',$user->id)}}"><i class="fa fa-edit"> Edit</i></a></td>
                    </tr>
                @endforeach
            @endif
        </tbody>
      </table>
@endsection


